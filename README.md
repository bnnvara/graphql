# BNNVARA\GraphQL

[![Code Coverage](https://scrutinizer-ci.com/b/bnnvara/graphql/badges/coverage.png?b=master)](https://scrutinizer-ci.com/b/bnnvara/graphql/?branch=master)
[![Build Status](https://scrutinizer-ci.com/b/bnnvara/graphql/badges/build.png?b=master)](https://scrutinizer-ci.com/b/bnnvara/graphql/build-status/master)

By [BNNVARA](https://werkenbij.bnnvara.nl/)

This package contains classes interfaces that wrap around [webonyx/graphql](https://github.com/webonyx/graphql-php). 

The main goal of this package is getting more structure for your graphql endpoints in bigger projects. It only contains 
functionality needed in our projects.
 
Miss some functionality? Feel free to send a pull request! 

## Installation

Via composer:
```
composer require bnnvara\graphql
```

## Examples

Examples can be found on the [examples](EXAMPLES.md). 