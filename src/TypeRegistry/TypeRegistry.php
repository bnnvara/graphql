<?php

namespace BNNVARA\GraphQL\TypeRegistry;

use GraphQL\Type\Definition\OutputType;

class TypeRegistry
{
    private $types = [];

    private $namedTypes = [];

    /**
     * @param OutputType $type
     * @param string $name
     *
     * @throws DuplicateNameException
     * @throws NameNotProvidedException
     */
    public function addType(OutputType $type, string $name)
    {
        if (array_key_exists($name, $this->types) === true) {
            throw new DuplicateNameException(sprintf('An other type with the name "%s" already exists', $name));
        }

        $this->types[$name] = $type;
        $this->namedTypes[$type->name] = $type;
    }

    /**
     * @param string $name
     *
     * @return OutputType
     * @throws TypeNotFoundException
     */
    final public function type(string $name): OutputType
    {
        if (array_key_exists($name, $this->types) === true) {
            return $this->types[$name];
        }

        if (array_key_exists($name, $this->namedTypes) === true) {
            return $this->namedTypes[$name];
        }

        throw new TypeNotFoundException('Requested type not found. Did you forget to add it to the registry?');
    }

    final public function __invoke($name): OutputType
    {
        return $this->type($name);
    }

    public function getTypes(): array
    {
        return $this->types;
    }
}
