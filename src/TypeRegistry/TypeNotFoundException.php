<?php

namespace BNNVARA\GraphQL\TypeRegistry;

use Exception;

class TypeNotFoundException extends Exception
{
}
