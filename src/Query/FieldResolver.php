<?php

namespace BNNVARA\GraphQL\Query;

use GraphQL\Type\Definition\ResolveInfo;

class FieldResolver
{
    /** @var ResolverInterface[] */
    private $resolvers = [];

    public function addResolver(QueryInterface $query)
    {
        $this->resolvers[] = $query->getResolver();
    }

    /** @inheritdoc */
    public function resolve($val, $args, $context, ResolveInfo $info)
    {
        $resolvers = array_filter($this->resolvers,
            function (ResolverInterface $resolver) use ($val, $args, $context, $info) {
                return $resolver->isSupported($val, $args, $context, $info);
            });

        if (count($resolvers) === 0) {
            throw new FieldResolverException(sprintf('No resolver found for field (%s)', $info->fieldName));
        } elseif (count($resolvers) > 1) {
            throw new FieldResolverException(sprintf('Multiple Resolver found for field (%s)', $info->fieldName));
        }

        /** @var ResolverInterface $resolver */
        $resolver = current($resolvers);

        return $resolver->resolve($val, $args, $context, $info);
    }
}
