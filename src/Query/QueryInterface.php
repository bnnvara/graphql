<?php

namespace BNNVARA\GraphQL\Query;

interface QueryInterface
{
    /** @return array */
    public function getQuery() : array;

    /** @return ResolverInterface */
    public function getResolver() : ResolverInterface;
}
