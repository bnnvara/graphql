<?php

namespace BNNVARA\GraphQL\Query;

use BNNVARA\GraphQL\TypeRegistry\TypeRegistry;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;

class QueryBuilder
{
    /** @var array */
    private $fields = [];

    /** @var FieldResolver */
    private $fieldResolver;

    /** @var TypeRegistry */
    private $typeRegistry;

    public function __construct(FieldResolver $fieldResolver, TypeRegistry $typeRegistry)
    {
        $this->fieldResolver = $fieldResolver;
        $this->typeRegistry = $typeRegistry;
    }

    public function getQuery(): ObjectType
    {
        $config = [
            'name' => 'Query',
            'fields' => $this->fields,
            'resolveField' => function ($val, $args, $context, ResolveInfo $info) {
                return $this->fieldResolver->resolve($val, $args, $context, $info);
            }
        ];

        $object = new ObjectType($config);

        $this->typeRegistry->addType($object, 'Query');

        return $object;
    }

    public function hasQuery(): bool
    {
        return count($this->fields) > 0;
    }

    public function addQuery(QueryInterface $query)
    {
        $this->fields[] = $query->getQuery();
    }
}
