<?php

namespace BNNVARA\GraphQL\Query;

use GraphQL\Type\Definition\ResolveInfo;

interface ResolverInterface
{
    /**
     * @param string $val
     * @param array $args
     * @param string $context
     * @param ResolveInfo $info
     */
    public function resolve($val, $args, $context, ResolveInfo $info);

    /**
     * @param string $val
     * @param array $args
     * @param string $context
     * @param ResolveInfo $info
     *
     * @return boolean
     */
    public function isSupported($val, $args, $context, ResolveInfo $info) : bool;
}
