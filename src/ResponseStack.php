<?php

declare(strict_types=1);

namespace BNNVARA\GraphQL;

class ResponseStack
{
    /** @var Response[] */
    private array $responses = [];

    public function addResponse(Response $response)
    {
        $this->responses[] = $response;
    }

    public function responses(): array
    {
        return $this->responses;
    }
}
