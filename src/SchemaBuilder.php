<?php

namespace BNNVARA\GraphQL;

use BNNVARA\GraphQL\Mutation\MutationBuilder;
use BNNVARA\GraphQL\Query\QueryBuilder;
use BNNVARA\GraphQL\TypeRegistry\TypeRegistry;
use GraphQL\Type\Schema;
use GraphQL\Type\SchemaConfig;

class SchemaBuilder
{
    /** @var QueryBuilder */
    private $queryCollector;

    /** @var MutationBuilder */
    private $mutationBuilder;

    /** @var callable */
    private $typeRegistry;

    public function __construct(
        QueryBuilder $queryCollector,
        MutationBuilder $mutationBuilder,
        TypeRegistry $typeRegistry
    ) {
        $this->queryCollector = $queryCollector;
        $this->mutationBuilder = $mutationBuilder;
        $this->typeRegistry = $typeRegistry;
    }

    public function getSchema(): Schema
    {
        $config = $this->getSchemaConfig();

        if ($this->mutationBuilder->hasMutation()) {
            $config->setMutation($this->mutationBuilder->getMutation());
        }

        if ($this->queryCollector->hasQuery()) {
            $config->setQuery($this->queryCollector->getQuery());
        }

        $config->setTypes($this->typeRegistry->getTypes());
        $config->setTypeLoader($this->typeRegistry);

        return new Schema($config);
    }

    protected function getSchemaConfig(): SchemaConfig
    {
        return SchemaConfig::create();
    }
}
