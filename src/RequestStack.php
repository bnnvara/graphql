<?php

declare(strict_types=1);

namespace BNNVARA\GraphQL;

class RequestStack
{
    private array $requests = [];

    public function __construct(string $payload)
    {
        $content = json_decode($payload, true);
        if (false === array_key_exists('query', $content)) {
            foreach ($content as $subRequestPayload) {
                $this->requests[] = new Request(json_encode($subRequestPayload));
            }
        } else {
            $this->requests[] = new Request($payload);
        }
    }

    /** @return Request[] */
    public function requests(): array
    {
        return $this->requests;
    }
}
