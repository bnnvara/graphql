<?php

declare(strict_types=1);

namespace BNNVARA\GraphQL;

class Response
{
    const RESPONSE_SUCCESS = 200;

    private string $data;
    private int $statusCode;

    public function __construct(string $data, int $statusCode)
    {
        $this->data = $data;
        $this->statusCode = $statusCode;
    }

    public function hasErrors(): bool
    {
        return array_key_exists('errors', json_decode($this->data, true)) &&
            count(json_decode($this->data, true)['errors']) > 0;
    }

    public function statusCode(): int
    {
        return $this->statusCode;
    }

    public function data(): string
    {
        return $this->data;
    }
}
