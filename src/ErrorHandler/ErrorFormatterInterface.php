<?php

namespace BNNVARA\GraphQL\ErrorHandler;

use GraphQL\Error\Error;

interface ErrorFormatterInterface
{
    public function __invoke(Error $error): array;
}
