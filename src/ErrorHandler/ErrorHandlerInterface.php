<?php

namespace BNNVARA\GraphQL\ErrorHandler;

interface ErrorHandlerInterface
{
    public function __invoke(array $errors): array;
}
