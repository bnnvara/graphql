<?php

namespace BNNVARA\GraphQL\ErrorHandler;

class ErrorHandler implements ErrorHandlerInterface
{
    /** @var ErrorFormatterInterface */
    private $errorFormatter;

    public function __construct(ErrorFormatterInterface $errorFormatter)
    {
        $this->errorFormatter = $errorFormatter;
    }

    public function __invoke(array $errors): array
    {
        return array_map($this->errorFormatter, $errors);
    }
}
