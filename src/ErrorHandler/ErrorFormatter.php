<?php

namespace BNNVARA\GraphQL\ErrorHandler;

use GraphQL\Error\Error;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class ErrorFormatter implements ErrorFormatterInterface
{
    /** @var boolean */
    private $debug;

    public function __construct(bool $debug = false)
    {
        $this->debug = $debug;
    }

    public function __invoke(Error $error): array
    {
        $previousError = $error->getPrevious();
        if ($previousError instanceof InvalidArgumentException) {
            $errorArray = [
                'message' => $previousError->getMessage(),
                'category' => 'User input error',
                'code' => 400
            ];

            if ($this->debug) {
                $errorArray['path'] = $error->getPath();
            }

            return $errorArray;
        }

        throw $error;
    }
}
