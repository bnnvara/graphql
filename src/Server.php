<?php

declare(strict_types=1);

namespace BNNVARA\GraphQL;

use BNNVARA\GraphQL\EventDispatcher\EventDispatcherInterface;
use BNNVARA\GraphQL\EventDispatcher\Events;
use BNNVARA\GraphQL\EventDispatcher\PostHandleEvent;
use BNNVARA\GraphQL\EventDispatcher\PreHandleEvent;
use GraphQL\Server\StandardServer;

class Server
{
    private StandardServer $server;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        StandardServer $server,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->server = $server;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function handleRequest(RequestStack $requestStack)
    {
        $responseStack = new ResponseStack();

        foreach ($requestStack->requests() as $request) {
            $context = new RequestContext($request);

            $this->eventDispatcher->dispatch(new PreHandleEvent($context));
            if ($context->hasResponse()) {
                $responseStack->addResponse($context->response());
                continue;
            }

            $responseData = $this->server->executeRequest($request->payload())->toArray();
            $context->setResponse(
                new Response(
                    json_encode($responseData),
                    array_key_exists(
                        'error',
                        $responseData
                    ) ? $responseData['error']['code'] : Response::RESPONSE_SUCCESS
                )
            );

            $this->eventDispatcher->dispatch(new PostHandleEvent($context));
            $responseStack->addResponse($context->response());
        }

        return $responseStack;
    }
}
