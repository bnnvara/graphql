<?php

declare(strict_types=1);

namespace BNNVARA\GraphQL\EventDispatcher;

class EventDispatcher implements EventDispatcherInterface
{
    private $listeners = [];

    public function dispatch(Event $event): void
    {
        /** @var EventSubscriberInterface|callable $listener */
        foreach ($this->getListenersForEvent($event->name()) as $listener) {
            if ($listener['listener'] instanceof EventSubscriberInterface) {
                $method = $listener['method'];
                $listener['listener']->$method($event);
            } else {
                $listener['listener']($event);
            }

            if ($event->isPropagationStopped()) {
                break;
            }
        }
    }

    public function addSubscriber(EventSubscriberInterface $eventSubscriber): void
    {
        foreach ($eventSubscriber->getSubscribedEvents() as $event) {
            $this->listeners[$event['event']][$event['priority'] ?? EventPriorities::LOW][] = [
                'method' => $event['method'],
                'listener' => $eventSubscriber
            ];
        }
    }

    public function addListener(string $event, int $priority, callable $eventListener): void
    {
        $this->listeners[$event][$priority][] = [
            'listener' => $eventListener
        ];
    }

    public function getListenersForEvent(string $event): array
    {
        $eventListeners = [];
        if (array_key_exists($event, $this->listeners) === false) {
            return $eventListeners;
        }

        ksort($this->listeners[$event]);
        foreach ($this->listeners[$event] as $listeners) {
            $eventListeners = array_merge($eventListeners, $listeners);
        }

        return $eventListeners;
    }
}
