<?php

declare(strict_types=1);

namespace BNNVARA\GraphQL\EventDispatcher;

class PostHandleEvent extends Event
{
    public function name(): string
    {
        return Events::POST_HANDLE_REQUEST;
    }
}
