<?php

declare(strict_types=1);

namespace BNNVARA\GraphQL\EventDispatcher;

interface EventDispatcherInterface
{
    public function dispatch(Event $event): void;
}
