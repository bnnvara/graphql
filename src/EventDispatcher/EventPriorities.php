<?php

declare(strict_types=1);

namespace BNNVARA\GraphQL\EventDispatcher;

final class EventPriorities
{
    public const HIGH = 100;
    public const NORMAL = 200;
    public const LOW = 300;
}
