<?php

declare(strict_types=1);

namespace BNNVARA\GraphQL\EventDispatcher;

use BNNVARA\GraphQL\RequestContext;

abstract class Event
{
    private bool $propagating = true;
    private RequestContext $requestContext;

    public function __construct(RequestContext $requestContext)
    {
        $this->requestContext = $requestContext;
    }

    public function stopPropagation(): void
    {
        $this->propagating = false;
    }

    public function isPropagationStopped(): bool
    {
        return $this->propagating === false;
    }

    abstract public function name(): string;

    public function requestContext(): RequestContext
    {
        return $this->requestContext;
    }
}
