<?php

declare(strict_types=1);

namespace BNNVARA\GraphQL\EventDispatcher;

final class Events
{
    public const PRE_HANDLE_REQUEST = 'PRE_HANDLE_REQUEST';
    public const POST_HANDLE_REQUEST = 'POST_HANDLE_REQUEST';
}
