<?php

declare(strict_types=1);

namespace BNNVARA\GraphQL\EventDispatcher;

interface EventSubscriberInterface
{
    public function getSubscribedEvents(): array;
}
