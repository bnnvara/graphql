<?php

declare(strict_types=1);

namespace BNNVARA\GraphQL\EventDispatcher;

class PreHandleEvent extends Event
{
    public function name(): string
    {
        return Events::PRE_HANDLE_REQUEST;
    }
}
