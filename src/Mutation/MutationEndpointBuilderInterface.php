<?php

namespace BNNVARA\GraphQL\Mutation;

interface MutationEndpointBuilderInterface
{
    /**
     * @return Mutation
     */
    public function build(): Mutation;
}
