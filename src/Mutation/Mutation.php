<?php

namespace BNNVARA\GraphQL\Mutation;

use GraphQL\Type\Definition\Type;

class Mutation
{
    private $functionName;

    /** @var array */
    private $arguments;

    /** @var Type */
    private $type;

    private $resolver;

    private function __construct($functionName, array $arguments, Type $type, callable $resolver)
    {
        $this->functionName = $functionName;
        $this->arguments = $arguments;
        $this->type = $type;
        $this->resolver = $resolver;
    }

    public static function create($functionName, $arguments, $responseType, callable $resolver): Mutation
    {
        return new static(
            $functionName,
            $arguments,
            $responseType,
            $resolver
        );
    }

    public function generate() : array
    {
        return [
            'name' => $this->functionName,
            'args' => $this->arguments,
            'type' => $this->type,
            'resolve' => $this->resolver
        ];
    }
}
