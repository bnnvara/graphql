<?php

namespace BNNVARA\GraphQL\Mutation;

use GraphQL\Type\Definition\ObjectType;

class MutationBuilder
{
    private $endpointBuilders = [];

    public function getMutation() : ObjectType
    {
        return new ObjectType(
            [
                'name' => 'Mutation',
                'fields' => array_map(
                    function(MutationEndpointBuilderInterface $mutationEndpointBuilder) {
                        return $mutationEndpointBuilder->build()->generate();
                    },
                    $this->endpointBuilders
                )
            ]
        );
    }

    public function hasMutation(): bool
    {
        return count($this->endpointBuilders) > 0;
    }

    public function addMutationEndpoint(MutationEndpointBuilderInterface $endpointBuilder) : void
    {
        $this->endpointBuilders[] = $endpointBuilder;
    }
}
