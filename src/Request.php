<?php

declare(strict_types=1);

namespace BNNVARA\GraphQL;

use GraphQL\Server\OperationParams;

class Request
{
    private string $payload;

    public function __construct(string $payload)
    {
        $this->payload = $payload;
    }

    public function payload(): OperationParams
    {
        return OperationParams::create(json_decode($this->payload, true));
    }

    public function rawPayload(): string
    {
        return $this->payload;
    }
}
