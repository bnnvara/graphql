<?php

namespace BNNVARA\GraphQL;

use Assert\Assertion;
use Assert\InvalidArgumentException;
use BNNVARA\GraphQL\ErrorHandler\ErrorHandlerInterface;
use GraphQL\Server\ServerConfig;
use GraphQL\Server\StandardServer;
use GraphQL\Type\Schema;

class StandardServerFactory
{
    public static function create(
        Schema $schema,
        ErrorHandlerInterface $errorHandler,
        bool $queryBatching,
        array $validationRules
    ): StandardServer {
        try {
            Assertion::allIsCallable($validationRules);
        } catch(InvalidArgumentException $e) {
            throw new InvalidValidationRuleProvidedException('ValidationRule must be a callable');
        }

        return new StandardServer(
            ServerConfig::create(
                [
                    'queryBatching' => $queryBatching,
                    'validationRules' => $validationRules
                ]
            )
                ->setSchema($schema)
                ->setErrorsHandler($errorHandler)
        );
    }
}
