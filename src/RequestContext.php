<?php

declare(strict_types=1);

namespace BNNVARA\GraphQL;

use Error;

class RequestContext
{
    private Request $request;
    private Response $response;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function request(): Request
    {
        return $this->request;
    }

    public function setResponse(Response $response)
    {
        $this->response = $response;
    }

    public function response(): Response
    {
        if ($this->hasResponse() === false) {
            throw new ResponseNotSetException();
        }

        return $this->response;
    }

    public function hasResponse(): bool
    {
        try {
            $this->response;
        } catch (Error $e) {
            return false;
        }

        return true;
    }
}
