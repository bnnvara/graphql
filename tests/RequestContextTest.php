<?php

declare(strict_types=1);

namespace Tests\BNNVARA\GraphQL;

use BNNVARA\GraphQL\EventDispatcher\Events;
use BNNVARA\GraphQL\EventDispatcher\RequestContextException;
use BNNVARA\GraphQL\Request;
use BNNVARA\GraphQL\RequestContext;
use BNNVARA\GraphQL\Response;
use BNNVARA\GraphQL\ResponseNotSetException;
use PHPUnit\Framework\TestCase;

class RequestContextTest extends TestCase
{
    /** @test */
    public function requestContextKeepsOriginalRequest(): void
    {
        $request = new Request('{}');
        $requestContext = new RequestContext($request);

        $this->assertSame($request, $requestContext->request());
    }

    /** @test */
    public function responseIsSetAndCanBeRetrieved(): void
    {
        $response = new Response('{}', Response::RESPONSE_SUCCESS);
        $requestContext = new RequestContext(new Request('{}'));
        $requestContext->setResponse($response);

        $this->assertSame($response, $requestContext->response());
    }

    /** @test */
    public function anExceptionIsThrownIfResponseIsAskedButNotSet(): void
    {
        $this->expectException(ResponseNotSetException::class);

        $requestContext = new RequestContext(new Request('{}'));
        $requestContext->response();
    }

    /** @test */
    public function presenceOfResponseCanBeChecked(): void
    {
        $requestContext = new RequestContext(new Request('{}'));
        $this->assertFalse($requestContext->hasResponse());
    }
}
