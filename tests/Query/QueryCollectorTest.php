<?php

namespace Tests\BNNVARA\GraphQL\Query;

use BNNVARA\GraphQL\TypeRegistry\TypeRegistry;
use BNNVARA\GraphQL\Query\FieldResolver;
use BNNVARA\GraphQL\Query\QueryBuilder;
use BNNVARA\GraphQL\Query\QueryInterface;
use BNNVARA\GraphQL\Query\ResolverInterface;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;

class QueryCollectorTest extends TestCase
{
    /** @test */
    public function anObjectTypeIsCreatedWithGivenQueries()
    {
        $queryCollector = $this->getQueryCollector($this->getFieldResolver());

        $queryCollector->addQuery($this->getQueryInterface());

        $this->assertInstanceOf(ObjectType::class, $queryCollector->getQuery());
    }

    private function getQueryCollector(FieldResolver $resolver)
    {
        return new QueryBuilder($resolver, $this->getTypeRegistryMock());
    }

    private function getFieldResolver() : FieldResolver
    {
        $fieldResolver = $this->getMockBuilder(FieldResolver::class)->disableOriginalConstructor()->getMock();

        /** @var FieldResolver $fieldResolver */
        return $fieldResolver;
    }

    private function getQueryInterface()
    {
        return new class implements QueryInterface {
            public function getName() : string {
                return 'string';
            }

            public function getQuery() : array
            {
                return [
                    'name' => 'name',
                    'type' => Type::boolean()
                ];
            }

            public function getResolver() : ResolverInterface
            {
                return new class() implements ResolverInterface {

                    public function resolve($val, $args, $context, ResolveInfo $info)
                    {
                        return [];
                    }

                    public function isSupported($val, $args, $context, ResolveInfo $info) : boolean
                    {
                        return true;
                    }
                };
            }
        };
    }

    private function getTypeRegistryMock(): TypeRegistry
    {
        $typeRegistry = $this->getMockBuilder(TypeRegistry::class)->disableOriginalConstructor()->getMock();

        /** @var TypeRegistry $typeRegistry */
        return $typeRegistry;
    }
}
