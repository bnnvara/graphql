<?php

namespace Tests\BNNVARA\GraphQL\Query;

use BNNVARA\GraphQL\Query\FieldResolver;
use BNNVARA\GraphQL\Query\FieldResolverException;
use BNNVARA\GraphQL\Query\QueryInterface;
use BNNVARA\GraphQL\Query\ResolverInterface;
use GraphQL\Type\Definition\ResolveInfo;
use PHPUnit\Framework\TestCase;

class FieldResolverTest extends TestCase
{
    /** @test */
    public function resolverIsAddedAndReturnsValue()
    {
        $resolver = $this->getFieldResolver();
        $resolver->addResolver($this->getQueryInterface());

        $result = $resolver->resolve('', '', '', $this->getResolveInfoMock());

        $this->assertEquals('lalala', $result);
    }

    /** @test */
    public function anExceptionIsThrownWhenMultipleResolversFound()
    {
        $this->expectException(FieldResolverException::class);

        $resolver = $this->getFieldResolver();

        $resolver->addResolver($this->getQueryInterface());
        $resolver->addResolver($this->getQueryInterface());

        $resolver->resolve('val', ['args'], 'context', $this->getResolveInfoMock());
    }

    /** @test */
    public function anExceptionIsThrownIfNoResolverIsFound()
    {
        $this->expectException(FieldResolverException::class);

        $resolver = $this->getFieldResolver();
        $resolver->resolve('val', ['args'], 'context', $this->getResolveInfoMock());
    }

    private function getFieldResolver()
    {
        return new FieldResolver();
    }

    private function getQueryInterface()
    {
        return new class implements QueryInterface {
            /** @return array */
            public function getQuery() : array
            {
                return [];
            }

            /** @return ResolverInterface */
            public function getResolver() : ResolverInterface
            {
                return new class implements ResolverInterface {
                    public function resolve($val, $args, $context, ResolveInfo $info)
                    {
                        return 'lalala';
                    }

                    public function isSupported($val, $args, $context, ResolveInfo $info) : bool
                    {
                        return true;
                    }
                };
            }
        };
    }

    /** @return ResolveInfo */
    public function getResolveInfoMock()
    {
        return $this->getMockBuilder(ResolveInfo::class)->disableOriginalConstructor()->getMock();
    }
}
