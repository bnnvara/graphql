<?php

namespace Tests\BNNVARA\GraphQL\Query;

use BNNVARA\GraphQL\TypeRegistry\TypeRegistry;
use BNNVARA\GraphQL\Query\FieldResolver;
use BNNVARA\GraphQL\Query\QueryBuilder;
use BNNVARA\GraphQL\Query\QueryInterface;
use PHPUnit\Framework\TestCase;

class QueryBuilderTest extends TestCase
{
    /**
     * @test
     * @dataProvider queryCountProvider
     */
    public function queriesCanBeAdded(int $queryCount): void
    {
        $queryBuilder = $this->getQueryBuilder($this->getFieldResolver());
        $this->assertFalse($queryBuilder->hasQuery());

        for($i=0;$i<$queryCount;$i++) {
            $queryBuilder->addQuery($this->getQueryInterface());
        }
        $this->assertTrue($queryBuilder->hasQuery());

        $query = $queryBuilder->getQuery();

        $this->assertCount($queryCount, $query->config['fields']);
    }

    public function queryCountProvider()
    {
        return [
            [
                3
            ],
            [
                2
            ],
            [
                100
            ]
        ];
    }

    private function getQueryBuilder(FieldResolver $fieldResolver): QueryBuilder
    {
        return new QueryBuilder($fieldResolver, $this->getTypeRegistryWithTypeCall());
    }

    private function getTypeRegistryWithTypeCall(): TypeRegistry
    {
        $registry = $this->getMockBuilder(TypeRegistry::class)->disableOriginalConstructor()->getMock();

        $registry->expects($this->once())
            ->method('addType');

        /** @var TypeRegistry $registry */
        return $registry;
    }

    private function getQueryInterface(): QueryInterface
    {
        $queryMock = $this->getMockBuilder(QueryInterface::class)->getMock();

        /** @var QueryInterface $queryMock */
        return $queryMock;
    }

    private function getFieldResolver(): FieldResolver
    {
        $fieldResolverMock = $this->getMockBuilder(FieldResolver::class)->getMock();

        /** @var FieldResolver $fieldResolverMock */
        return $fieldResolverMock;
    }
}
