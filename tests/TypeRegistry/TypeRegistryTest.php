<?php

namespace Tests\BNNVARA\GraphQL\TypeRegistry;

use BNNVARA\GraphQL\TypeRegistry\DuplicateNameException;
use BNNVARA\GraphQL\TypeRegistry\TypeNotFoundException;
use BNNVARA\GraphQL\TypeRegistry\TypeRegistry;
use GraphQL\Type\Definition\FloatType;
use GraphQL\Type\Definition\IDType;
use GraphQL\Type\Definition\IntType;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;

class TypeRegistryTest extends TestCase
{
    /** @test */
    public function typeCanBeAddedToTypeRegistry(): void
    {
        $typeRegistry = new TypeRegistry();

        $typeRegistry->addType(Type::float(), 'float');

        $typeReflection = new \ReflectionProperty($typeRegistry, 'types');
        $typeReflection->setAccessible(true);
        $value = $typeReflection->getValue($typeRegistry);
        $this->assertCount(1, $value);

        $namedTypeReflection = new \ReflectionProperty($typeRegistry, 'types');
        $namedTypeReflection->setAccessible(true);
        $value = $namedTypeReflection->getValue($typeRegistry);
        $this->assertCount(1, $value);

        $typeRegistry->addType(Type::id(), 'id');
        $typeRegistry->addType(Type::string(), 'string');
        $typeRegistry->addType(Type::int(), 'int');

        $typeReflection = new \ReflectionProperty($typeRegistry, 'types');
        $typeReflection->setAccessible(true);
        $typesCollection = $typeReflection->getValue($typeRegistry);
        $this->assertCount(4, $typesCollection);

        $typeReflection = new \ReflectionProperty($typeRegistry, 'namedTypes');
        $typeReflection->setAccessible(true);
        $typesCollection = $typeReflection->getValue($typeRegistry);
        $this->assertCount(4, $typesCollection);
    }

    /** @test */
    public function anExceptionIsThrownIfTypeWithSameNameIsAdded()
    {
        $this->expectException(DuplicateNameException::class);

        $typeRegistry = new TypeRegistry();

        $typeRegistry->addType(Type::float(), 'name');
        $typeRegistry->addType(Type::int(), 'name');
    }

    /** @test */
    public function aTypeCanBeRetrievedByItsName(): void
    {
        $typeRegistry = new TypeRegistry();

        $typeRegistry->addType(Type::float(), FloatType::class);
        $typeRegistry->addType(Type::int(), IntType::class);
        $typeRegistry->addType(Type::id(), IDType::class);

        $this->assertInstanceOf(FloatType::class, $typeRegistry->type('Float'));
        $this->assertInstanceOf(FloatType::class, $typeRegistry->type(FloatType::class));
    }

    /** @test */
    public function typeRegistryCanBeUsedAsCallable(): void
    {
        $typeRegistry = new TypeRegistry();

        $typeRegistry->addType(Type::float(), FloatType::class);
        $typeRegistry->addType(Type::int(), IntType::class);
        $typeRegistry->addType(Type::id(), IDType::class);

        $this->assertInstanceOf(FloatType::class, $typeRegistry(FloatType::class));
        $this->assertInstanceOf(FloatType::class, $typeRegistry('Float'));
    }

    /** @test */
    public function anExceptionIsThrownIfRequestedTypeDoesNotExistInRegistry(): void
    {
        $this->expectException(TypeNotFoundException::class);

        $typeRegistry = new TypeRegistry();

        $typeRegistry->type(FloatType::class);
    }

    /** @test */
    public function namedTypesCanBeRetrieved(): void
    {
        $typeRegistry = new TypeRegistry();

        $types = [
            FloatType::class => Type::float(),
            IntType::class => Type::int()
        ];

        foreach($types as $name => $type) {
            $typeRegistry->addType($type, $name);
        }

        $this->assertEquals($types, $typeRegistry->getTypes());

    }
}
