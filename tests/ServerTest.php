<?php

namespace Tests\BNNVARA\GraphQL;

use BNNVARA\GraphQL\EventDispatcher\Event;
use BNNVARA\GraphQL\EventDispatcher\EventDispatcherInterface;
use BNNVARA\GraphQL\EventDispatcher\Events;
use BNNVARA\GraphQL\RequestContext;
use BNNVARA\GraphQL\RequestStack;
use BNNVARA\GraphQL\Response;
use BNNVARA\GraphQL\Server;
use GraphQL\Executor\ExecutionResult;
use GraphQL\Server\StandardServer;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ServerTest extends TestCase
{
    /**  @test */
    public function itSupportsQueryBatching(): void
    {
        $server = $this->getServer(
            $this->getStandardServerWithExecuteRequestCall(new ExecutionResult(), 2),
            $this->getEventDispatcher()
        );

        $responseStack = $server->handleRequest(new RequestStack('[{"query": ""},{"query": ""}]'));
        $this->assertCount(2, $responseStack->responses());
    }

    /** @test */
    public function itSupportsSingleQuery()
    {
        $server = $this->getServer(
            $this->getStandardServerWithExecuteRequestCall(new ExecutionResult()),
            $this->getEventDispatcher()
        );

        $responseStack = $server->handleRequest(new RequestStack('{"query": ""}'));

        $this->assertCount(1, $responseStack->responses());
    }

    /** @test */
    public function eventDispatcherIsCalledBeforeAndAfterExecutingRequest(): void
    {
        $server = $this->getServer(
            $this->getStandardServerWithExecuteRequestCall(new ExecutionResult()),
            $this->getEventCalledEventDispatcher()
        );

        $server->handleRequest(new RequestStack('{"query": ""}'));
    }

    /** @test */
    public function eventDispatcherStopsEventExecutionWithErrorResponse(): void
    {
        $server = $this->getServer(
            $this->getStandardServer(),
            $this->getEventDispatcherThatAddsResponseAndStopsPropagation()
        );

        $responseStack = $server->handleRequest(new RequestStack('{"query": ""}'));

        $this->assertCount(1, $responseStack->responses());
        $this->assertTrue($responseStack->responses()[0]->hasErrors());
    }

    private function getServer(StandardServer $server, EventDispatcherInterface $eventDispatcher): Server
    {
        return new Server($server, $eventDispatcher);
    }

    private function getStandardServerWithExecuteRequestCall($executionResult, $numberOfCalls = 1): StandardServer
    {
        /** @var MockObject $server */
        $server = $this->getStandardServer();

        $server->expects($this->exactly($numberOfCalls))
            ->method('executeRequest')
            ->will($this->returnValue($executionResult));

        /** @var StandardServer $server */
        return $server;
    }

    private function getStandardServer(): StandardServer
    {
        $server = $this->getMockBuilder(StandardServer::class)->disableOriginalConstructor()->getMock();

        /** @var StandardServer $server */
        return $server;
    }

    private function getEventDispatcher(): EventDispatcherInterface
    {
        $eventDispatcher = $this->getMockBuilder(EventDispatcherInterface::class)->getMock();

        /** @var EventDispatcherInterface $eventDispatcher */
        return $eventDispatcher;
    }

    private function getEventCalledEventDispatcher(): EventDispatcherInterface
    {
        /** @var MockObject $eventDispatcher */
        $eventDispatcher = $this->getEventDispatcher();

        $eventDispatcher->expects($this->exactly(2))->method('dispatch')->with(
            $this->isInstanceOf(Event::class)
        );

        /** @var EventDispatcherInterface $eventDispatcher */
        return $eventDispatcher;
    }

    private function getEventDispatcherThatAddsResponseAndStopsPropagation(): EventDispatcherInterface
    {
        return new class implements EventDispatcherInterface {

            public function dispatch(Event $event): void
            {
                if ($event->name() === Events::PRE_HANDLE_REQUEST) {
                    $event->requestContext()->setResponse(
                        new Response(
                            '{"errors": [{ "message": "Error message", "category": "User input error", "code": 400, "path": [ "brand" ]}]}',
                            200
                        )
                    );
                    $event->stopPropagation();
                }
            }
        };
    }
}
