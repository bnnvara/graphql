<?php

declare(strict_types=1);

namespace Tests\BNNVARA\GraphQL;

use BNNVARA\GraphQL\EventDispatcher\EventDispatcher;
use BNNVARA\GraphQL\RequestStack;
use BNNVARA\GraphQL\Response;
use BNNVARA\GraphQL\SchemaBuilder;
use BNNVARA\GraphQL\Server;
use GraphQL\Server\ServerConfig;
use GraphQL\Server\StandardServer;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Schema;
use PHPUnit\Framework\TestCase;

class GraphQLIntegrationTest extends TestCase
{
    /** @test */
    public function requestIsExecuted(): void
    {
        $server = new Server(
            new StandardServer(
                ServerConfig::create()
                    ->setSchema(
                        new Schema(
                            [
                                'query' => new ObjectType(
                                    [
                                        'name' => 'Query',
                                        'fields' => [
                                            'user' => [
                                                'type' => Type::string(),
                                                'args' => [
                                                    'name' => [
                                                        'type' => Type::string()
                                                    ]
                                                ],
                                                'resolve' => function($data, array $args) {
                                                    return $args['name'];
                                                }
                                            ]
                                        ]
                                    ]
                                )
                            ]
                        )
                    )
            ),
            new EventDispatcher()
        );

        $result = $server->handleRequest(new RequestStack('{
  "query": "query just_a_name($name: String) {user(name: $name)}",
  "variables": {
    "name": "James"
  }
}
'));

        $this->assertSame('{"data":{"user":"James"}}', $result->responses()[0]->data());
        $this->assertSame(Response::RESPONSE_SUCCESS, $result->responses()[0]->statusCode());
    }
}
