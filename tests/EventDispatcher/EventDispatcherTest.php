<?php

declare(strict_types=1);

namespace Tests\BNNVARA\GraphQL\EventDispatcher;

use BNNVARA\GraphQL\EventDispatcher\EventDispatcher;
use BNNVARA\GraphQL\EventDispatcher\EventPriorities;
use BNNVARA\GraphQL\EventDispatcher\Events;
use BNNVARA\GraphQL\EventDispatcher\EventSubscriberInterface;
use BNNVARA\GraphQL\EventDispatcher\PostHandleEvent;
use BNNVARA\GraphQL\EventDispatcher\PreHandleEvent;
use BNNVARA\GraphQL\Request;
use BNNVARA\GraphQL\RequestContext;
use BNNVARA\GraphQL\Response;
use PHPUnit\Framework\TestCase;

class EventDispatcherTest extends TestCase
{
    /** @test */
    public function dispatchIsStoppedIfPropagationStopped(): void
    {
        $eventDispatcher = new EventDispatcher();
        $eventDispatcher->addListener(
            Events::POST_HANDLE_REQUEST,
            EventPriorities::HIGH,
            function(PostHandleEvent $event) {
                $event->requestContext()->setResponse(new Response('{}', Response::RESPONSE_SUCCESS));
                $event->stopPropagation();
            }
        );
        $neverCalledEventSubscriber = new DummyEventSubscriber();
        $neverCalledEventSubscriber->addEvent(Events::POST_HANDLE_REQUEST, EventPriorities::NORMAL, 'neverCalledMethod');
        $eventDispatcher->addSubscriber($neverCalledEventSubscriber);

        $requestContext = new RequestContext(new Request('{}'));

        $eventDispatcher->dispatch(new PostHandleEvent($requestContext));

        $this->assertFalse($neverCalledEventSubscriber->isCalled());
    }

    /** @test */
    public function eventsAreDispatched(): void
    {
        $eventDispatcher = new EventDispatcher();

        $eventDispatcher->addListener(
            Events::PRE_HANDLE_REQUEST,
            EventPriorities::LOW,
            function(PreHandleEvent $event) {
                $event->requestContext()->setResponse(new Response('{}', Response::RESPONSE_SUCCESS));
                $event->stopPropagation();
            }
        );

        $calledEventSubscriber = $this->createEmptySubscriber(Events::PRE_HANDLE_REQUEST);
        $eventDispatcher->addSubscriber($calledEventSubscriber);

        $eventSubscriberForOtherEvent = $this->createEmptySubscriber(Events::POST_HANDLE_REQUEST);
        $eventDispatcher->addSubscriber($eventSubscriberForOtherEvent);

        $secondCalledEventSubscriber = $this->createEmptySubscriber(Events::PRE_HANDLE_REQUEST);
        $eventDispatcher->addSubscriber($secondCalledEventSubscriber);

        $requestContext = new RequestContext(new Request('{}'));
        $eventDispatcher->dispatch($event = new PreHandleEvent($requestContext));

        $this->assertTrue($calledEventSubscriber->isCalled());
        $this->assertFalse($eventSubscriberForOtherEvent->isCalled());
        $this->assertTrue($secondCalledEventSubscriber->isCalled());
        $this->assertTrue($event->isPropagationStopped());
    }

    /** @test */
    public function eventSubscriberCanBeAdded(): void
    {
        $eventDispatcher = new EventDispatcher();

        $highPriorityEventSubscriber = $this->createEventSubscriber();
        $highPriorityEventSubscriber->addEvent(Events::POST_HANDLE_REQUEST, EventPriorities::HIGH, 'method');
        $eventDispatcher->addSubscriber($highPriorityEventSubscriber);

        $this->assertCount(1, $eventDispatcher->getListenersForEvent(Events::POST_HANDLE_REQUEST));
        $listeners = $eventDispatcher->getListenersForEvent(Events::POST_HANDLE_REQUEST);

        $this->assertCount(1, $listeners);
        $this->assertSame($highPriorityEventSubscriber, $listeners[0]['listener']);
    }

    /** @test */
    public function eventListenerCanBeAdded(): void
    {
        $eventDispatcher = new EventDispatcher();

        $eventDispatcher->addListener(Events::POST_HANDLE_REQUEST, EventPriorities::HIGH, function() {});

        $listeners = $eventDispatcher->getListenersForEvent(Events::POST_HANDLE_REQUEST);

        $this->assertArrayHasKey('listener', $listeners[0]);
    }

    /** @test */
    public function listenersForRightEventAreReturned(): void
    {
        $eventDispatcher = new EventDispatcher();
        $postHandleEventSubscriber = $this->createEventSubscriber();
        $postHandleEventSubscriber->addEvent(Events::POST_HANDLE_REQUEST, EventPriorities::HIGH, 'just-a-method');
        $eventDispatcher->addSubscriber($postHandleEventSubscriber);

        $preHandleEventSubscriber = $this->createEventSubscriber();
        $preHandleEventSubscriber->addEvent(Events::PRE_HANDLE_REQUEST, EventPriorities::HIGH, 'just-a-method');
        $eventDispatcher->addSubscriber($preHandleEventSubscriber);

        $listeners = $eventDispatcher->getListenersForEvent(Events::POST_HANDLE_REQUEST);
        $this->assertCount(1, $listeners);
        $this->assertSame($postHandleEventSubscriber, $listeners[0]['listener']);
    }

    /** @test */
    public function listenersAreReturnedInTheRightOrder(): void
    {
        $eventDispatcher = new EventDispatcher();

        $lowPriorityEventSubscriber = $this->createEventSubscriber();
        $lowPriorityEventSubscriber->addEvent(Events::PRE_HANDLE_REQUEST, EventPriorities::LOW, 'method-low');

        $eventDispatcher->addSubscriber($lowPriorityEventSubscriber);

        $highPriorityEventSubscriber = $this->createEventSubscriber();
        $highPriorityEventSubscriber->addEvent(Events::PRE_HANDLE_REQUEST, EventPriorities::HIGH, 'method-high');
        $eventDispatcher->addSubscriber($highPriorityEventSubscriber);

        $normalPriorityEventSubscriber = $this->createEventSubscriber();
        $normalPriorityEventSubscriber->addEvent(Events::PRE_HANDLE_REQUEST, EventPriorities::NORMAL, 'method-normal');
        $eventDispatcher->addSubscriber($normalPriorityEventSubscriber);

        $listeners = $eventDispatcher->getListenersForEvent(Events::PRE_HANDLE_REQUEST);
        $this->assertSame($highPriorityEventSubscriber, $listeners[0]['listener']);
        $this->assertSame($normalPriorityEventSubscriber, $listeners[1]['listener']);
        $this->assertSame($lowPriorityEventSubscriber, $listeners[2]['listener']);

        $listeners = $eventDispatcher->getListenersForEvent(Events::POST_HANDLE_REQUEST);
        $this->assertCount(0, $listeners);
    }

    private function createEventSubscriber()
    {
        return new class implements EventSubscriberInterface {

            private $events = [];

            public function getSubscribedEvents(): array
            {
                return $this->events;
            }

            public function addEvent(string $event, int $priority, string $method)
            {
                $this->events[] = [
                    'event' => $event,
                    'priority' => $priority,
                    'method' => $method
                ];
            }

            public function __call(string $method, array $arguments) {}
        };
    }

    private function createEmptySubscriber(string $event): DummyEventSubscriber
    {
        $dummyEventSubscriber = new DummyEventSubscriber();
        $dummyEventSubscriber->addEvent($event, EventPriorities::HIGH, 'request');

        return $dummyEventSubscriber;
    }
}
