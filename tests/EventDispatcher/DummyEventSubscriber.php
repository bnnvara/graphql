<?php

declare(strict_types=1);

namespace Tests\BNNVARA\GraphQL\EventDispatcher;

use BNNVARA\GraphQL\EventDispatcher\EventSubscriberInterface;
use BNNVARA\GraphQL\RequestContext;
use BNNVARA\GraphQL\Response;

class DummyEventSubscriber implements EventSubscriberInterface
{
    private bool $isCalled = false;
    private array $events = [];

    public function getSubscribedEvents(): array
    {
        return $this->events;
    }

    public function addEvent(string $event, int $priority, string $method): void
    {
        $this->events[] = [
            'event' => $event,
            'priority' => $priority,
            'method' => $method
        ];
    }

    public function __call(string $method, array $arguments)
    {
        /** @var RequestContext $requestContext */
        $requestContext = reset($arguments);

        switch($method) {
            case 'stopPropagation':
                $requestContext->setResponse(new Response());
                break;
            case 'addResponse':
                break;
            case 'addResponseAndStopPropagation':
                break;
        }

        $this->isCalled = true;
    }

    public function isCalled(): bool
    {
        return $this->isCalled;
    }
}
