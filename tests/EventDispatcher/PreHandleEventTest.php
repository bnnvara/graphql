<?php

declare(strict_types=1);

namespace Tests\BNNVARA\GraphQL\EventDispatcher;

use BNNVARA\GraphQL\EventDispatcher\Events;
use BNNVARA\GraphQL\EventDispatcher\PreHandleEvent;
use BNNVARA\GraphQL\RequestContext;
use PHPUnit\Framework\TestCase;

class PreHandleEventTest extends TestCase
{
    /** @test */
    public function propagationCanBeStopped(): void
    {
        $event = new PreHandleEvent($this->createRequestContext());

        $this->assertFalse($event->isPropagationStopped());

        $event->stopPropagation();

        $this->assertTrue($event->isPropagationStopped());
    }

    /** @test */
    public function contextCanBeRetrieved(): void
    {
        $requestContext = $this->createRequestContext();

        $event = new PreHandleEvent($requestContext);

        $this->assertSame($requestContext, $event->requestContext());
    }

    /** @test */
    public function nameIsSet(): void
    {
        $event = new PreHandleEvent($this->createRequestContext());

        $this->assertSame(Events::PRE_HANDLE_REQUEST, $event->name());
    }

    private function createRequestContext(): RequestContext
    {
        $context = $this->getMockBuilder(RequestContext::class)->disableOriginalConstructor()->getMock();

        /** @var RequestContext $context */
        return $context;
    }
}
