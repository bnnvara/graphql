<?php

namespace Tests\BNNVARA\GraphQL\ErrorHandler;

use Assert\InvalidArgumentException;
use BNNVARA\GraphQL\ErrorHandler\ErrorFormatter;
use Exception;
use GraphQL\Error\Error;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class ErrorFormatterTest extends TestCase
{
    /** @test */
    public function noDebugInformationIsGivenForNonDebugEnvironment()
    {
        $errorFormatter = new ErrorFormatter(false);

        $error = $errorFormatter(
            $this->getErrorWithPreviousException(
                new InvalidArgumentException('error-message', 400, __DIR__, 'value')
            )
        );

        $this->assertCount(3, $error);
        $this->assertEquals('error-message', $error['message']);
        $this->assertEquals(400, $error['code']);
        $this->assertEquals('User input error', $error['category']);
    }

    /** @test */
    public function debugInformationIsGivenForDebugEnvironment()
    {
        $errorFormatter = new ErrorFormatter(true);

        $error = $errorFormatter(
            $this->getErrorWithPreviousException(
                new InvalidArgumentException('error-message', 400, __DIR__, 'value')
            )
        );

        $this->assertCount(4, $error);
        $this->assertEquals('error-message', $error['message']);
        $this->assertEquals('User input error', $error['category']);
        $this->assertArrayHasKey('path', $error);
        $this->assertEquals(400, $error['code']);
    }

    /** @test */
    public function errorIsRethrownIfNonInvalidArgumentException()
    {
        $this->expectException(Error::class);

        $errorFormatter = new ErrorFormatter('prod');
        $errorFormatter($this->getErrorWithPreviousException(new Exception()));
    }

    /** @return Error */
    private function getErrorWithPreviousException(Exception $exception)
    {
        return new Error('', null, null, [], null, $exception);
    }
}
