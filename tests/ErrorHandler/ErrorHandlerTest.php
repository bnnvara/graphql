<?php

namespace Tests\BNNVARA\GraphQL\ErrorHandler;

use BNNVARA\GraphQL\ErrorHandler\ErrorFormatterInterface;
use BNNVARA\GraphQL\ErrorHandler\ErrorHandler;
use GraphQL\Error\Error;
use PHPUnit\Framework\TestCase;

class ErrorHandlerTest extends TestCase
{
    /** @test */
    public function errorsAreDelegatedToErrorFormatter()
    {
        $errors = $this->getErrorHandler($this->getErrorFormatterInterface())([$this->getErrorMock()]);

        $this->assertTrue(is_array($errors));
    }

    private function getErrorHandler(ErrorFormatterInterface $errorFormatter): ErrorHandler
    {
        return new ErrorHandler($errorFormatter);
    }

    private function getErrorFormatterInterface(): ErrorFormatterInterface
    {
        $formatterMock = $this->getMockBuilder(ErrorFormatterInterface::class)->getMock();

        $formatterMock->expects($this->any())
            ->method('__invoke')
            ->will($this->returnValue(["error message"]));

        /** @var ErrorFormatterInterface $formatterMock */
        return $formatterMock;
    }

    private function getErrorMock(): Error
    {
        $errorMock = $this->getMockBuilder(Error::class)->disableOriginalConstructor()->getMock();

        /** @var Error $errorMock */
        return $errorMock;
    }
}
