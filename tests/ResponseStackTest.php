<?php

declare(strict_types=1);

namespace Tests\BNNVARA\GraphQL;

use BNNVARA\GraphQL\Response;
use BNNVARA\GraphQL\ResponseStack;
use PHPUnit\Framework\TestCase;

class ResponseStackTest extends TestCase
{
    /** @test */
    public function responsesCanBeAddedToResponseStack(): void
    {
        $responseStack = new ResponseStack();

        $responseStack->addResponse(new Response('{}', 200));
        $this->assertCount(1, $responseStack->responses());

        $responseStack->addResponse(new Response('{}', 200));
        $this->assertCount(2, $responseStack->responses());
    }
}
