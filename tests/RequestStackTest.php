<?php

declare(strict_types=1);

namespace Tests\BNNVARA\GraphQL;

use BNNVARA\GraphQL\Request;
use BNNVARA\GraphQL\RequestStack;
use GraphQL\Server\OperationParams;
use PHPUnit\Framework\TestCase;

class RequestStackTest extends TestCase
{
    /** @test */
    public function requestsAreSplitInSeperateRequest(): void
    {
        $requestStack = new RequestStack('[{"query": ""},{"query": ""}]');

        $this->assertCount(2, $requestStack->requests());
        $this->assertContainsOnlyInstancesOf(Request::class, $requestStack->requests());
    }

    /** @test */
    public function requestIsStoredInRequestStack(): void
    {
        $requestStack = new RequestStack('{"query": ""}');

        $this->assertCount(1, $requestStack->requests());
        $this->assertSame('{"query": ""}', $requestStack->requests()[0]->rawPayload());
        $this->assertInstanceOf(OperationParams::class, $requestStack->requests()[0]->payload());
        $this->assertContainsOnlyInstancesOf(Request::class, $requestStack->requests());
    }
}
