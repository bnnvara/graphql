<?php

namespace Tests\BNNVARA\GraphQL;

use BNNVARA\GraphQL\ErrorHandler\ErrorHandlerInterface;
use BNNVARA\GraphQL\InvalidValidationRuleProvidedException;
use BNNVARA\GraphQL\StandardServerFactory;
use GraphQL\Server\StandardServer;
use GraphQL\Type\Schema;
use GraphQL\Validator\Rules\ValidationRule;
use PHPUnit\Framework\TestCase;

class StandardServerFactoryTest extends TestCase
{
    use PrivatePropertyManipulator;

    /** @test */
    public function aServerInstanceIsReturned()
    {
        $server = StandardServerFactory::create(
            $this->getSchema(),
            $this->getErrorHandlerInterface(),
            false,
            []
        );

        $this->assertInstanceOf(StandardServer::class, $server);
    }

    /** @test */
    public function validationRulesCanBeAdded(): void
    {
        $validationRules = [
            function() {},
            $this->createValidationRule()
        ];

        $server = StandardServerFactory::create(
            $this->getSchema(),
            $this->getErrorHandlerInterface(),
            false,
            $validationRules
        );

        $this->assertEquals($validationRules, $this->getByReflection($server, 'config')->getValidationRules());
    }

    /** @test */
    public function anExceptionIsThrownIfValidationRulesContainNonCallables(): void
    {
        $this->expectException(InvalidValidationRuleProvidedException::class);
        $this->expectExceptionMessage('ValidationRule must be a callable');

        $validationRules = [
            'not-a-callable'
        ];

        StandardServerFactory::create(
            $this->getSchema(),
            $this->getErrorHandlerInterface(),
            false,
            $validationRules
        );
    }

    private function getSchema(): Schema
    {
        $schema = $this->getMockBuilder(Schema::class)->disableOriginalConstructor()->getMock();

        /** @var Schema $schema */
        return $schema;
    }

    private function getErrorHandlerInterface(): ErrorHandlerInterface {
        $errorHandler = $this->getMockBuilder(ErrorHandlerInterface::class)->getMock();

        /** @var ErrorHandlerInterface $errorHandler */
        return $errorHandler;
    }

    private function createValidationRule(): ValidationRule
    {
        $validationRule = $this->getMockBuilder(ValidationRule::class)->disableOriginalConstructor()->getMock();

        /** @var ValidationRule $validationRule */
        return $validationRule;
    }
}
