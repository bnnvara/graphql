<?php

declare(strict_types=1);

namespace Tests\BNNVARA\GraphQL;

use BNNVARA\GraphQL\Response;
use PHPUnit\Framework\TestCase;

class ResponseTest extends TestCase
{
    /** @test */
    public function responseContentCanBeSet(): void
    {
        $responseData = '
            {
              "data": {
                "brand": null
              },
              "errors": [
                {
                  "message": "error 1",
                  "category": "User input error",
                  "code": 400,
                  "path": [
                    "brand"
                  ]
                },
                {
                  "message": "error 2",
                  "category": "User input error",
                  "code": 400,
                  "path": [
                    "brand"
                  ]
                }
              ]
            }';

        $response = new Response($responseData, Response::RESPONSE_SUCCESS);

        $this->assertTrue($response->hasErrors());
        $this->assertJsonStringEqualsJsonString($responseData, $response->data());
        $this->assertSame(Response::RESPONSE_SUCCESS, $response->statusCode());
    }

    /** @test */
    public function contentOfEmptyResponseIsStoredCorrectly(): void
    {
        $response = new Response('{}', Response::RESPONSE_SUCCESS);

        $this->assertEquals('{}', $response->data());
    }
}
