<?php

declare(strict_types=1);

namespace Tests\BNNVARA\GraphQL;

use BNNVARA\GraphQL\Request;
use GraphQL\Server\OperationParams;
use PHPUnit\Framework\TestCase;

class RequestTest extends TestCase
{
    /** @test */
    public function requestKeepsItsContent(): void
    {
        $request = new Request('[]');

        $this->assertSame('[]', $request->rawPayload());
    }

    /** @test */
    public function requestReturnsOperationParams(): void
    {
        $request = new Request('{}');

        $this->assertInstanceOf(OperationParams::class, $request->payload());
    }
}
