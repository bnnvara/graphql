<?php

namespace Tests\BNNVARA\GraphQL\Mutation;

use BNNVARA\GraphQL\Mutation\Mutation;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;

class MutationTest extends TestCase
{
    /** @test */
    public function endpointIsCreated()
    {
        $this->assertInstanceOf(Mutation::class, Mutation::create('', [], Type::boolean(), $this->getCallable()));
    }

    /** @test */
    public function generateReturnsArrayWithKeys()
    {
        $generateResult = (Mutation::create('', [], Type::boolean(), $this->getCallable()))->generate();

        $keys = ['name', 'args', 'type', 'resolve'];
        array_walk($keys, function($key) use ($generateResult)  {
            $this->assertArrayHasKey($key, $generateResult);
        });
    }

    private function getCallable()
    {
        return new class {
            public function __invoke() {}
        };
    }
}
