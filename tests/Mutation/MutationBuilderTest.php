<?php

namespace Tests\BNNVARA\GraphQL\Mutation;

use BNNVARA\GraphQL\Mutation\Mutation;
use BNNVARA\GraphQL\Mutation\MutationBuilder;
use BNNVARA\GraphQL\Mutation\MutationEndpointBuilderInterface;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use ReflectionProperty;

class MutationBuilderTest extends TestCase
{
    /** @test */
    public function mutationEndpointsCanBeAdded()
    {
        $builder = $this->getMutationBuilder();
        $this->assertFalse($builder->hasMutation());

        $builder->addMutationEndpoint($this->getMutationEndpointBuilderInterface());


        $reflection = new ReflectionProperty(get_class($builder), 'endpointBuilders');
        $reflection->setAccessible(true);
        $this->assertCount(1, $reflection->getValue($builder));
        $this->assertTrue($builder->hasMutation());
    }

    /** @test */
    public function mutationReturnsObjectType()
    {
        $builder = $this->getMutationBuilder();

        $builder->addMutationEndpoint($this->getMutationEndpointBuilderInterface());

        $this->assertInstanceOf(ObjectType::class, $builder->getMutation());
    }

    private function getMutationBuilder() : MutationBuilder
    {
        return new MutationBuilder();
    }

    private function getMutationEndpointBuilderInterface() : MutationEndpointBuilderInterface
    {
        $endpoint = Mutation::create('', [], Type::boolean(), function() {});

        return new class($endpoint) implements MutationEndpointBuilderInterface {

            private $endpoint;

            public function __construct(Mutation $endpoint) {

                $this->endpoint = $endpoint;
            }

            public function build(): Mutation
            {
                return $this->endpoint;
            }
        };
    }
}
