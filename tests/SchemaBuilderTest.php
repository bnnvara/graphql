<?php

namespace Tests\BNNVARA\GraphQL;

use BNNVARA\GraphQL\Mutation\MutationBuilder;
use BNNVARA\GraphQL\Query\QueryBuilder;
use BNNVARA\GraphQL\SchemaBuilder;
use BNNVARA\GraphQL\TypeRegistry\TypeRegistry;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Schema;
use GraphQL\Type\SchemaConfig;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class SchemaBuilderTest extends TestCase
{
    /** @test */
    public function builderReturnsSchema()
    {
        $schema = $this->createSchemaBuilder(
            $this->createQueryBuilder(true),
            $this->createMutationBuilder(true),
            $this->getTypeRegistryMock()
        )->getSchema();

        $this->assertInstanceOf(Schema::class, $schema);
        $this->assertEquals("Mutation", $schema->getConfig()->getMutation()->config['name']);
        $this->assertTrue(is_callable($schema->getConfig()->getTypeLoader()));
    }

    /**
     * @test
     * @dataProvider useCaseEmptyBuildersProvider
     */
    public function emptyBuildersAreNoCalledForSchema(bool $hasMutations, bool $hasQueries): void
    {
        $schemaBuilder = $this->createSchemaBuilderWithMockedSchemaConfig(
            $this->createSchemaConfig($hasMutations, $hasQueries),
            $this->createMutationBuilder($hasMutations),
            $this->createQueryBuilder($hasQueries)
        );

        $schemaBuilder->getSchema();
    }

    public function useCaseEmptyBuildersProvider(): array
    {
        return [
            [true, true],
            [true, false],
            [false, false],
            [false, true]
        ];
    }

    private function createSchemaConfig(bool $hasMutationCall, bool $hasQueryCall): SchemaConfig
    {
        $schemaConfig = $this->getMockBuilder(SchemaConfig::class)->disableOriginalConstructor()->getMock();

        if ($hasMutationCall) {
            $schemaConfig->expects($this->once())
                ->method('setMutation')
                ->will($this->returnValue([]));
        } else {
            $schemaConfig->expects($this->never())
                ->method('setMutation');
        }

        if ($hasQueryCall) {
            $schemaConfig->expects($this->once())
                ->method('setQuery')
                ->will($this->returnValue($schemaConfig));
        } else {
            $schemaConfig->expects($this->never())
                ->method('setQuery');
        }

        $schemaConfig->expects($this->once())
            ->method('setTypeLoader');

        /** @var SchemaConfig $schemaConfig */
        return $schemaConfig;
    }

    private function createSchemaBuilder(
        QueryBuilder $queryBuilder,
        MutationBuilder $mutationBuilder,
        TypeRegistry $typeRegistry
    ): SchemaBuilder {
        return new SchemaBuilder($queryBuilder, $mutationBuilder, $typeRegistry);
    }

    public function getTypeRegistryMock(): TypeRegistry
    {
        $typeRegistry = $this->getMockBuilder(TypeRegistry::class)->disableOriginalConstructor()->getMock();

        /** @var TypeRegistry $typeRegistry */
        return $typeRegistry;
    }

    private function getTypeRegistryWithGetTypesCall(): TypeRegistry
    {
        /** @var MockObject $typeRegistry */
        $typeRegistry = $this->getTypeRegistryMock();

        $typeRegistry->expects($this->once())
            ->method('getTypes')
            ->will($this->returnValue([]));

        /** @var TypeRegistry $typeRegistry */
        return $typeRegistry;
    }

    private function createQueryBuilder(bool $hasQuery): QueryBuilder
    {
        $queryBuilderMock = $this->getMockBuilder(QueryBuilder::class)->disableOriginalConstructor()->getMock();

        $queryBuilderMock->expects($this->once())->method('hasQuery')->will($this->returnValue($hasQuery));

        /** @var QueryBuilder $queryBuilderMock */
        return $queryBuilderMock;
    }

    private function createMutationBuilder(bool $hasMutation): MutationBuilder
    {
        $mutationBuilderMock = $this->getMockBuilder(MutationBuilder::class)->disableOriginalConstructor()->getMock();

        $mutationBuilderMock->expects($this->once())->method('hasMutation')->will($this->returnValue($hasMutation));
        if ($hasMutation) {
            $mutationBuilderMock->expects($this->any())
                ->method('getMutation')
                ->will($this->returnValue(new ObjectType(['name' => 'Mutation'])));
        }

        /** @var MutationBuilder $mutationBuilderMock */
        return $mutationBuilderMock;
    }

    private function createSchemaBuilderWithMockedSchemaConfig(
        SchemaConfig $schemaConfig,
        MutationBuilder $mutationBuilder,
        QueryBuilder $queryBuilder
    ): SchemaBuilder {
        $schemaBuilder = $this->getMockBuilder(SchemaBuilder::class)
            ->setConstructorArgs(
                [
                    $queryBuilder,
                    $mutationBuilder,
                    $this->getTypeRegistryWithGetTypesCall()
                ]
            )
            ->onlyMethods(['getSchemaConfig'])
            ->getMock();

        $schemaBuilder->expects($this->any())
            ->method('getSchemaConfig')
            ->willReturn($schemaConfig);

        /** @var SchemaBuilder $schemaBuilder */
        return $schemaBuilder;
    }
}
