# Examples

This library is a wrapper around the [webonyx/graphql](https://github.com/webonyx/graphql-php) library. For detailed 
information about this library we suggest looking at the docs of this package. Some basic knowledge of this library is 
required to use this package.

## Query
This example will show how to migrate a query implemented in webonyx to this package.

The following example comes from the documentation of webonyx and adds the echo query to the schema. In the rest of this
document we wil refer to this code example as 'original query'.

    $queryType = new ObjectType([
        'name' => 'Query',
        'fields' => [
            'echo' => [
                'type' => Type::string(),
                'args' => [
                    'message' => ['type' => Type::string()],
                ],
                'resolve' => function ($root, $args) {
                    return $args['message'];
                }
            ],
        ],
    ]);
    
    $schema = new Schema([
        'query' => $queryType
    ]);
    
    ...
    
    GraphQL::executeQuery($schema, ..., ..., ..., ...);
    
Below is shown how to implement this example with the BNNVARA/graphql package.

First an EchoQuery class is created. This class must implement the QueryInterface. If you look at the implementation 
below some things should look familiar. First have a look at the example. Below an explanation is given.

    class EchoQuery implements QueryInterface
    {
        /** @var string */
        private $name = 'echo';
    
        /** @inheritdoc */
        public function getQuery() : array
        {
            return [
                'name' => $this->name,
                'type' => Type::string()
            ];
        }
    
        /** @inheritdoc */
        public function getResolver() : ResolverInterface
        {
            return new class($this->name) implements ResolverInterface
            {
                /** @var string */
                private $name;
    
                public function __construct(string $name)
                {
                    $this->name = $name;
                }
                
                public function isSupported($val, $args, $context, ResolveInfo $info) : bool
                {
                    return $info->fieldName === $this->name;
                }
    
                public function resolve($val, $args, $context, ResolveInfo $info)
                {
                    return $args['message'];
                }
            };
        }
    }

The name parameter in the EchoQuery is given the value 'echo', similar to the first entry in the 'fields' query of the 
original query. Next the getQuery() method returns the values of the 'echo' array in the original query. Notice the 
resolve key is not returned by this function. For this an extra method is added. The getResolver() method. This method 
returns a ResolverInterface. 

This ResolverInterface defines two methods (and a constructor). The 'isSupported' method checks whether the requested 
query belongs to this resolver. In this example we only use one query, but normally there will be many queries and many 
resolvers. 
The second method 'resolve' contains the same code as the function of the 'resolve' key in the original query. 

### QueryBuilder
Now the EchoQuery needs to be added to the schema. For this the QueryBuilder is created. In the QueryBuilder
all queries that have to be part of the schema must be added using the 'addQuery()' method. It is the responsibility of 
the QueryBuilder to build a valid array the Schema expects.  

The following example shows how this is done.

    $queryBuilder = new QueryBuilder();
    
    // The next line should be repeated for every Query needed by the Schema
    $queryBuilder->addQuery(new EchoQuery());
    
Since the next step is the same for a mutation the next chapter will explain how to create a Mutation. Afterwards the 
creation of a schema is explained.

## Mutation
This example will show how to migrate a mutation implemented in webonyx to this package.

The following example comes from the documentation of webonyx and adds the Calc Mutation to the schema. In the rest of 
this document we wil refer to this code example as 'original mutation'.

    $mutationType = new ObjectType([
        'name' => 'Calc',
        'fields' => [
            'sum' => [
                'type' => Type::int(),
                'args' => [
                    'x' => ['type' => Type::int()],
                    'y' => ['type' => Type::int()],
                ],
                'resolve' => function ($root, $args) {
                    return $args['x'] + $args['y'];
                },
            ],
        ],
    ]);
    
    $schema = new Schema([
        'query' => $queryType
    ]);
    
    ...
        
    GraphQL::executeQuery($schema, ..., ..., ..., ...);

Below is shown how to implement this example with the BNNVARA/graphql package.

First an CalcMutation class (implementing MuatationEndpointBuilderInterface) is created. If you look at the 
implementation below some things should look familiar. First have a look at the example. Below an explanation is given.

    class CalcMutation implements MutationEndpointBuilderInterface
    {
        public function build(): Endpoint
        {
            return Endpoint::create(
                'Calc',
                [
                    'x' => Type::int(),
                    'y' => Type::int(),
                ],
                Type::int(),
                $this->getCallable()
            );
        }
    
        private function getCallable() : callable
        {
            return function ($root, $args) {
                return $args['x'] + $args['y'];
            };
        }
    }

The CalcMutation class has one task and that is returning an instance of EndPoint. The EndPoint::create() method expects 
three parameters. The first is the name. This is the value of the 'name' key in the original example. The second are the 
arguments. This is the value of the 'args' key in the example. The third parameter is the return type. The fourth 
parameters is the resolver that executes the actual business logic.

### MutationBuilder
To add the CalcMutation to the Schema the MutationBuilder is needed. This MutationBuilder is used to collect all 
mutations and build one configuration array needed by the Schema. The MutationBuilder is used as follows:

    $mutationBuilder = new MutationBuilder();
    
    // The next line should be repeated for every Mutation needed by the Schema
    $mutationBuilder->addMutationEndPoint(new CalcMutation());
    
Next chapter will describe how to create the Schema.

## Building the Schema
Now we have a QueryBuilder and an MutationBuilder. These two classes are the buildingblocks for a Schema. To create a
Schema the SchemaBuilder is used. The SchemaBuilder is a simple wrapper around the Schema class. It can be used like 
this:

    $schemaBuilder = new SchemaBuilder(
        $queryBuilder,
        $schemaBuilder
    );
    
    $schema = $schemaBuilder->getSchema();
    
The `getSchema()` method returns a Schema as defined by the Webonyx package. It is possible to use this schema with the 
`GraphQL::executeQuery()` method. Next chapters are optional (but recommended).

## ServerFactory
The webonyc package comes with a `StandardServer` class. The server instance has some advantages compared to the static 
`GraphQL::executeQuery()`. For example, QueryBatching and custom error handling can be used. This package comes with a 
basic StandardServerFactory. This factory class can be used as follows:

    $turnOnQueryBatching = true;
    $errorHandlerInterface = new ErrorHandler(new ErrorFormatter());

    $standardServer = StandardServerFactory::create($schema, $errorHandlerInterface, $turnOnQueryBatching);
    
    $payload = 'insert some json graphql query or mutation here';
    $standardServer->handleRequest($payload);
    
This package comes with a basic ErrorHandler class using an included ErrorFormatter class. Both classes implement 
an interface and can be replaced by custom written instances.